import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-childthree',
  templateUrl: './childthree.component.html',
  styleUrls: ['./childthree.component.css']
})
export class ChildthreeComponent implements OnInit {
  //messageFromChild: string = '';

  @Input() childthreeName!: string;
  @Output() parentMessage = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    //console.log(this.childthreeName);
  }

  getGreeting(message: string) {
    this.parentMessage.emit(message);
    //this.messageFromChild = message;
  }

}
