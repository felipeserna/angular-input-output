import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-childfour',
  templateUrl: './childfour.component.html',
  styleUrls: ['./childfour.component.css']
})
export class ChildfourComponent implements OnInit {
  //messageFromChild: string = '';

  @Input() childfourName!: string;
  @Output() parentMessage = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    //console.log(this.childfourName);
  }

  getGreeting(message: string) {
    this.parentMessage.emit(message);
    //this.messageFromChild = message;
  }

  

}
