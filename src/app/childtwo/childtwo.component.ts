import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-childtwo',
  templateUrl: './childtwo.component.html',
  styleUrls: ['./childtwo.component.css']
})
export class ChildtwoComponent implements OnInit {
  //messageFromChild: string = '';

  @Input() childtwoName!: string;
  @Output() parentMessage = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    //console.log(this.childtwoName);
  }

  getGreeting(message: string) {
    this.parentMessage.emit(message);
    //this.messageFromChild = message;
  }

}
