import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  messageFromChild: string = '';
  messageFromOldMan: string = 'Hi great-great-great-grandson (@Input())';
  //title = 'homework-one';

  getGreeting(messageFromChild: string) {
    this.messageFromChild = messageFromChild;
  }

  
}
